package com.example.vuejsbridge;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.github.lzyzsd.jsbridge.BridgeHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.BridgeWebViewClient;
import com.github.lzyzsd.jsbridge.CallBackFunction;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.net.http.SslError;

public class MainActivity extends AppCompatActivity {

    private TextView textViewTest;
    private BridgeWebView bridgeWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    /**
     * 初始化各种View
     */

    private void initView() {
        textViewTest=findViewById(R.id.activity_jsbridge_textViewtest);
        bridgeWebView=findViewById(R.id.activity_jsbridge_bridgewebview);
        bridgeWebView.setWebViewClient(new BridgeWebViewClient(bridgeWebView) {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();  // 接受所有网站的证书
            }
        });
        bridgeWebView.getSettings().setBuiltInZoomControls(true); //显示放大缩小 controler
        bridgeWebView.getSettings().setSupportZoom(true); //可以缩放
        bridgeWebView.getSettings().setDomStorageEnabled(true);//开启DOM
        bridgeWebView.loadUrl("https://xxxxx/");//h5地址

        //Android 通过 JSBridge 调用 JS
        textViewTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bridgeWebView.callHandler("monitorTestData", "h5你好，这是我Android主动发送数据给你，请你接收一下！！！（监听monitorTestData）", new CallBackFunction() {
                    @Override
                    public void onCallBack(String data) {
                        Toast.makeText(MainActivity.this, data, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        //JS 通过 JSBridge 调用 Android
        bridgeWebView.registerHandler("getAppData", new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                //JS传递给Android
                Toast.makeText(MainActivity.this,  data, Toast.LENGTH_LONG).show();
                //Android返回给JS的消息
                function.onCallBack("h5你好，这是你调用我Android的方法，现在我回调给你哟（回调getAppData）");
            }
        });
    }
}