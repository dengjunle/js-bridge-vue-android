import Vue from 'vue'
import App from './App.vue'

import JsBridge from "./utils/JsBridge"

Vue.prototype.$jsbridge = JsBridge;

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
